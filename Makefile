PACKAGENAME=racketmq
MAINCOLLECT=racketmq
COLLECTS=$(MAINCOLLECT)

all: setup

clean:
	find . -name compiled -type d | xargs rm -rf
	rm -rf htmldocs

setup:
	raco setup $(COLLECTS)

link:
	raco pkg install --link -n $(PACKAGENAME) $$(pwd)

unlink:
	raco pkg remove $(PACKAGENAME)

htmldocs:
	raco scribble \
		--html \
		--dest htmldocs \
		--dest-name index \
		++main-xref-in \
		--redirect-main http://docs.racket-lang.org/ \
		\
		$(MAINCOLLECT)/scribblings/$(MAINCOLLECT).scrbl

pages:
	@(git branch -v | grep -q gh-pages || (echo local gh-pages branch missing; false))
	@echo
	@git branch -av | grep gh-pages
	@echo
	@(echo 'Is the branch up to date? Press enter to continue.'; read dummy)
	git clone -b gh-pages . pages

publish: htmldocs pages
	rm -rf pages/*
	cp -r htmldocs/. pages/.
	(cd pages; git add -A)
	-(cd pages; git commit -m "Update $$(date +%Y%m%d%H%M%S)")
	(cd pages; git push)
	rm -rf pages

###########################################################################
#
# Makefile targets `letsencrypt-register` and `letsencrypt-renew`
# request LIVE certificates.
#
# To get the first certificate,
#
#     make letsencrypt-register DOMAIN=... ADMINEMAIL=...
#
# Add the following to `app`'s crontab to attempt renewal on the 2nd of
# each month:
#
#     17 17 2 * * (cd /home/app/live/rmq; make letsencrypt-renew)
#
# Add the following to `root`'s crontab to restart `nginx` after
# potential certificate renewal:
#
#     19 17 2 * * service nginx restart
#

letsencrypt-register: letsencrypt/etc/live/$(DOMAIN)
letsencrypt/etc/live/$(DOMAIN):
	[ -n "$$ADMINEMAIL" ]
	[ -n "$$DOMAIN" ]
	(certbot certonly \
		--config-dir $(CURDIR)/letsencrypt/etc \
		--work-dir $(CURDIR)/letsencrypt/tmp \
		--logs-dir $(CURDIR)/letsencrypt/log \
		--email $(ADMINEMAIL) \
		--agree-tos \
		--webroot \
		-w $(CURDIR)/racketmq/htdocs \
		-d $(DOMAIN) \
		)

letsencrypt-renew:
	(certbot renew \
		--config-dir $(CURDIR)/letsencrypt/etc \
		--work-dir $(CURDIR)/letsencrypt/tmp \
		--logs-dir $(CURDIR)/letsencrypt/log \
		)

###########################################################################

post-receive-hook:
	( cd ../syndicate/racket && git pull && \
		( raco pkg update --auto --update-deps syndicate || true) )
	raco make racketmq/hub.rkt
	svc -du racketmq
