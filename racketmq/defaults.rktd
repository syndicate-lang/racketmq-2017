;;===========================================================================
;;                       Configuration for RacketMQ
;;---------------------------------------------------------------------------
;;
;; Each configuration entry should be a list with a symbol (the "key")
;; as its first item followed by zero or more items.
;;
;; The configuration file is automatically reread by the server when
;; it is changed: if you need to make changes to it, consider doing so
;; atomically by producing an updated configuration file and using
;; rename(2) / mv(1) to activate it.

;;===========================================================================
;; REQUIRED:
;;===========================================================================

;;---------------------------------------------------------------------------
;; Exactly one "canonical-baseurl" key, containing a URL string naming
;; the base URL used for constructing URLs that are given out to third
;; parties, such as subscription endpoints for upstream hubs to use.
;;
;; This is *just* for URL construction, and does NOT create any HTTP
;; listeners. Those are configured with "http-listener" keys,
;; documented below.
;;
(canonical-baseurl "http://localhost:7827/")

;;---------------------------------------------------------------------------
;; At least one "http-listener" key. These cause an HTTP server to be
;; spun up for each mentioned port number. Traffic will only be
;; accepted for HTTP Host headers mentioned in these keys.
;;
(http-listener "localhost" 7827)
;; (http-listener "localhost" 80)
;; (http-listener "www.example.com" 7827)
;;
;; etc.

;;===========================================================================
;; FINE TUNING:
;;===========================================================================

;;---------------------------------------------------------------------------
;; When performing discovery / upstream content retrieval, the hub
;; will follow this many redirects before deciding it has had enough.
;;
;; (max-upstream-redirects 5)

;;---------------------------------------------------------------------------
;; If a subscription request arrives with no specified
;; `hub.lease_seconds`, then `default-lease` is used. If a requested
;; lease duration exceeds `max-lease`, then `max-lease` is used
;; instead.
;;
;; (default-lease 86400) ;; 86400 seconds = one day
;; (max-lease 604800) ;; 604800 seconds = one week

;;---------------------------------------------------------------------------
;; Upstream topics will be polled from time to time, according to the
;; settings of each local subscription to the topic. Subscriptions may
;; supply `hub.poll_interval_seconds` as either a number or the string
;; "none". If no `hub.poll_interval_seconds` is supplied in a
;; subscription, `default-poll-interval` is used. If all subscriptions
;; to an upstream topic have "none" as their poll interval, no polling
;; will occur; otherwise, polling will occur at the fastest requested
;; rate, but never more frequently than every `min-poll-interval`
;; seconds.
;;
;; (min-poll-interval 60) ;; seconds
;; (default-poll-interval "none") ;; seconds, or "none"

;;---------------------------------------------------------------------------
;; If subscription to an upstream hub fails immediately, we will
;; schedule a retry in this many seconds.
;;
;; (subscription-retry-delay 600) ;; seconds

;;---------------------------------------------------------------------------
;; Subscriptions last until explicitly terminated by an unsubscription
;; request, implicitly terminated by lease expiry, or implicitly
;; terminated by sustained delivery failure.
;;
;; When the hub sends a *content distribution request* (see the WebSub
;; spec) to a subscription's callback, if a success response is
;; returned, the delivery is considered successful.
;;
;; Otherwise, the hub begins an exponential backoff process, with an
;; initial delay of `initial-retry-delay` seconds, increasing by a
;; factor of `retry-delay-multiplier` (subject to a cap of
;; `max-retry-delay` seconds) with each subsequent attempt until
;; `max-delivery-retries` attempts have been made. At that point, if
;; all attempts to deliver the particular content distribution request
;; have failed, the request is considered a "dead letter" and is
;; effectively discarded. Once a request has either succeeded or
;; become a dead letter, the hub continues with any further pending
;; content distribution requests for the subscription.
;;
;; If more than `max-dead-letters` dead letters pile up for a
;; subscription, the subscription is considered too damaged to
;; continue to exist, and is terminated.
;;
;; (max-dead-letters 10)
;; (max-delivery-retries 10)
;; (initial-retry-delay 5.0) ;; seconds
;; (retry-delay-multiplier 1.618)
;; (max-retry-delay 30) ;; seconds
