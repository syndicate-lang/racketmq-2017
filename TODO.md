
✓ HEAD for racketmq topic links

✓ Proper async verification

✓ Split implementation into multiple modules

✓ Remove "unbounded" lease duration

✓ Configurability

✓ Explain codebase layout in README

CORS (ugh ugh ugh)

Secrets

Allow secret of a subscription to be changed

Durable subscriptions and other kinds of hub state

Make second-and-subsequent subscriptions to remote topics behave the same as first subscriptions:
 - randomize the first poll of the content
 - never do a poll if the poll interval is "none" for the topic as a whole

Local topic history retention and browsing

For topic creation: allow creation of distinct read and write URLs for
one topic, to allow cap-style control of write- and read-permission.

WebSockets: allow subscribe/unsubscribe along the websocket.
